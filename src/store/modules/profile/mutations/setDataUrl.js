/**
 * Set Data URL
 */
const setDataUrl = (state, _dataUrl) => {
    /* Set data URL. */
    state.dataUrl = _dataUrl
}

/* Export module. */
export default setDataUrl
