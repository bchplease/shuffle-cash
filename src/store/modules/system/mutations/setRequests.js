/**
 * Set Requests
 */
const setRequests = (state, _requests) => {
    /* Set requests. */
    state.requests = _requests
}

/* Export module. */
export default setRequests
